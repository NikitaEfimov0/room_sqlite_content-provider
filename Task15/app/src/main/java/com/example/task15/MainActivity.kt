package com.example.task15

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.task15.data.Contact
import com.example.task15.data.ContactViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var mContactViewModel: ContactViewModel
    private var checkPos:Boolean = true;
    lateinit var sharedPref:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mContactViewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        sharedPref = getSharedPreferences("isCan", MODE_PRIVATE)
        checkPos = sharedPref.getBoolean("ifCan", true)
        if(checkPos)
        getContacts()


    }
    @SuppressLint("Range")
    fun getContacts(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS) , 0)
        }
        val contentResolver: ContentResolver = contentResolver
        val uri: Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val cursor: Cursor? = contentResolver.query(uri, null, null, null, null)
        Log.d("Nothing", "${cursor?.count}")
        if(cursor?.count!! >0){
            while(cursor.moveToNext()){
                var name:String = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                var phone:String = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                insertDataToDatabase(name, phone)
            }
        }
        if(checkPos) {
            checkPos = false
            sharedPref.edit().putBoolean("ifCan", checkPos)
        }
    }

    fun insertDataToDatabase(str:String, num:String){
        val name = str
        val phone = num
        val contact: Contact = Contact(0, name, phone, "")
        mContactViewModel.addContact(contact)
    }

    fun deleteCurrentTable(){
        mContactViewModel.deleteAllContacts()
    }
}