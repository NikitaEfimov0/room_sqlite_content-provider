package com.example.task15.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addContact(contact:Contact)

    @Update
    suspend fun updateContact(contact: Contact)

    @Query("DELETE FROM contact_table")
    suspend fun deleteAllContacts()

    @Query("SELECT * FROM contact_table ORDER BY id ASC")
    fun readAllData():LiveData<List<Contact>>

    @Delete
    suspend fun deleteContact(contact:Contact)
}