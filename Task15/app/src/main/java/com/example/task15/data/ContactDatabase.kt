package com.example.task15.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Contact::class], version = 2, exportSchema = false)
abstract class ContactDatabase:RoomDatabase() {

    abstract fun contactDao():ContactDao

    companion object{

        val migration_1_2 = object: Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {

            }
        }
        @Volatile
        private var INSTANCE:ContactDatabase? = null

        fun getDatabase(context: Context):ContactDatabase{
            val tempInstance = INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ContactDatabase::class.java,
                    "contact_database"
                ).addMigrations(migration_1_2).build()
                INSTANCE = instance
                return instance
            }
        }
    }


}