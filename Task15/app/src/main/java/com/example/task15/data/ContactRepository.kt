package com.example.task15.data

import androidx.lifecycle.LiveData

class ContactRepository(private val contactDao:ContactDao) {
    val readAllData: LiveData<List<Contact>> = contactDao.readAllData()

    suspend fun updateContact(contact: Contact){
        contactDao.updateContact(contact)
    }

    suspend fun addContact(contact:Contact){
        contactDao.addContact(contact)
    }

    suspend fun deleteContact(contact: Contact){
        contactDao.deleteContact(contact)
    }

    suspend fun deleteAllContacts(){
        contactDao.deleteAllContacts()
    }
}