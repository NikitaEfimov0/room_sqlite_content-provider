package com.example.task15

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.task15.data.Contact
import com.example.task15.data.ContactViewModel
import com.example.task15.databinding.FragmentContactInfoBinding
import com.example.task15.databinding.FragmentMainBinding

class ContactInfo : Fragment() {
    private lateinit var binding: FragmentContactInfoBinding
    private lateinit var mContactInfo: ContactViewModel

    private val args by navArgs<ContactInfoArgs>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentContactInfoBinding.inflate(inflater)

        mContactInfo = ViewModelProvider(this)[ContactViewModel::class.java]

        binding.NameOnInfo.text = args.currentContact.Name
        binding.PhoneOnInfo.text = args.currentContact.phoneNumber
        binding.NotesOnInfo.setText(args.currentContact.Notes)

        binding.addNote.setOnClickListener {
            changeNote()
            Toast.makeText(requireContext(), "Заметка изменена", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.toMainFromInfo)
        }

        return binding.root
    }


    private fun changeNote(){
        val Notes = binding.NotesOnInfo.text.toString()
        val contact = Contact(args.currentContact.id, args.currentContact.Name, args.currentContact.phoneNumber, Notes)
        mContactInfo.updateContact(contact)
    }


    companion object {

        @JvmStatic
        fun newInstance() = ContactInfo()
    }
}