package com.example.task15.data

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "contact_table")
data class Contact(
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val Name:String,
    val phoneNumber:String,
    var Notes:String
):Parcelable{

    fun changeNote(string: String){
        Notes = string;
    }

}