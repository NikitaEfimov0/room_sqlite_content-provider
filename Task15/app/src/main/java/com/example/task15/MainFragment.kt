package com.example.task15
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.task15.databinding.FragmentMainBinding



import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.task15.data.ContactViewModel



class MainFragment : Fragment() {
    private lateinit var mContactViewModel:ContactViewModel
    private lateinit var adapter:ListAdapter
    lateinit var binding: FragmentMainBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMainBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ListAdapter()
        val recyclerView = binding.recycleView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        mContactViewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        mContactViewModel.readAllData.observe(viewLifecycleOwner) { contact ->
            adapter.setData(contact)
            Log.d("vales", adapter.itemCount.toString())
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = MainFragment()
    }
}