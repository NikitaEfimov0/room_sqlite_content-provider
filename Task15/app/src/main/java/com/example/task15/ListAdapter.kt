package com.example.task15

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.task15.data.Contact


class ListAdapter:RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var contactList = emptyList<Contact>()
    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val name = itemView.findViewById<TextView>(R.id.NameOnCard)
        val phone = itemView.findViewById<TextView>(R.id.PhoneOnCard)
        val notes = itemView.findViewById<TextView>(R.id.NotesOnCard)
        val card = itemView.findViewById<LinearLayout>(R.id.contactCard)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.contact_card, parent, false))

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = contactList[position]
        holder.name.text = currentItem.Name
        holder.phone.text = currentItem.phoneNumber
        holder.notes.text = currentItem.Notes

        holder.card.setOnClickListener {
            val action = MainFragmentDirections.toInfoFromMain(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(contact: List<Contact>){
        this.contactList = contact
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return contactList.size
    }
}